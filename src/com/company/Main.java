package com.company;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        String[] arr = readFromFile("test.txt");
        if (arr == null)
            throw new RuntimeException("Нет значений для сортировки");

        System.out.println("\nПо возрастанию");
        Stream.of(arr)
                .map(Integer::valueOf)
                .sorted()
                .forEach(s -> System.out.print(s + " "));
        System.out.println("\n\nПо убыванию");
        Stream.of(arr)
                .map(Integer::valueOf)
                .sorted(Comparator.reverseOrder())
                .forEach(s -> System.out.print(s + " "));
    }

    private static String[] readFromFile(String path) {
        try (FileReader reader = new FileReader(path)) {
            Scanner f = new Scanner(reader);
            if (f.hasNext())
                return Stream.of(f.next().split(","))
                        .map(String::trim)
                        .toArray(String[]::new);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
